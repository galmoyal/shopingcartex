const mongoose = require("mongoose");

const ProductSchema = new mongoose.Schema({
    title: String,
    price: Number,
    img: Number,
    shortDescription: String,
    longDescription: String,
    isFavorite: Boolean,
    createdAt: Date,
    updatedAt: Date,
    unitsInStock: Number,
    quantity: Number,
    votes: {
        upVotes: {
            upperLimit: Number,
            currentValue: Number,
        },
        downVotes: {
            lowerLimit: Number,
            currentValue: Number,
        },
    },
    author: {
        id: String,
        firstName: String,
        lastName: String,
        email: String
    }
});

const productModel = mongoose.model("products", ProductSchema);

module.exports = productModel
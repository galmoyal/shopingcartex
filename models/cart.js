const mongoose = require("mongoose");

const CartSchema = new mongoose.Schema({
    products: [
        {
            product_id: String,
            count: Number
        },
    ],
    TotalPrice: Number
});

const CartModel = mongoose.model("Cart", CartSchema);

module.exports = CartModel;
const productModel = require("../models/product")

module.exports = class ProductsController {
    constructor(app) {
        this.app = app;
        this.init()
    }

    init() {
        this.app.post("/products", async (req, res) => {
            await productModel.create(req.body);
            return res.json({ status: 200 });
        });

        this.app.get("/products", async (req, res) => {
            const filter = req.body.filter
            const products = await productModel.find(filter).exec();
            return res.json(products);
        });

        this.app.put("/products", async (req, res) => {
            const id = req.body._id
            const newValue = req.body.newValue
            let updatedFlight = await productModel.findByIdAndUpdate(id, newValue, { new: true }).exec();
            return res.json(updatedFlight);
        });

        this.app.delete("/products", async (req, res) => {
            const deleted = await productModel.findByIdAndDelete(req.body._id);
            if (deleted) {
                return res.json({ status: 200 });
            }
            return res.json({ status: 404 })
        });
    }
}
const cartModel = require("../models/cart")
const productModel = require("../models/product")

module.exports = class CartController {
    constructor(app) {
        this.app = app;
        this.init();
    }

    init() {
        this.app.post("/cart/checkout", async (req, res) => {
            let checkoutResponse = [];
            for (const product of req.body) {
                checkoutResponse.push({
                    title: product.title,
                    price: product.price,
                    quantity: product.quantity,
                    totalPrice: product.price * product.quantity
                })
            }
            return res.json(checkoutResponse);
        })

        this.app.post("/cart/addItem", async (req, res) => {
            let cartToUpdate = await cartModel.findById(req.body._id).exec();

            if (!cartToUpdate) {
                //not found cart
                return res.json({ status: 404 })
            }

            alreadyExists = false;
            for (const product of cartToUpdate.products) {
                if (product.product_id == req.body.productId) {
                    product.count += req.body.count;
                    alreadyExists = true;
                }
            }
            if (!alreadyExists) {
                // new product
                let productToAdd = {
                    product_id: req.body.productId,
                    count: req.body.count
                };

                cartToUpdate.products.push(productToAdd);
            }
            calculateNewTotalPrice(cartToUpdate);
            // check newTotalPrice makes sense

            await cartModel.save();
            return res.json({ status: 200 });
        });

        this.app.post("/cart/decreaseItemCount", async (req, res) => {
            let cartToUpdate = await cartModel.findById(req.body._id).exec();

            if (!cartToUpdate) {
                //not found cart
                return res.json({ status: 404 })
            }

            // product exists in cart
            for (const product of cartToUpdate.products) {
                if (product.product_id == req.body.productId) {
                    product.count -= req.body.count;
                    calculateNewTotalPrice(cartToUpdate);
                    await cartToUpdate.save();
                    return res.json({ status: 200 })
                }
            }

            return res.json({ status: 404 })
        })
    }


    calculateNewTotalPrice(cartToUpdate) {
        let totalPrice = 0;
        for (const cartProduct of cartToUpdate.products) {
            let populatedProduct = productModel.findById(cartProduct.product_id);
            totalPrice += populatedProduct.price * cartProduct.count;
        }
        cartToUpdate.TotalPrice = totalPrice;
    }
}
const mongoose = require("mongoose");
const path = require('path')
const seed = require("./models/seed");


const cors = require("cors");
const corsOptions ={
    origin:'http://localhost:3000', 
    credentials:true,            //access-control-allow-credentials:true
    optionSuccessStatus:200
}
let express = require("express")
const http = require("http")
const CartController = require("./controllers/cart.controller");
const ProductsController = require("./controllers/product.controller")
function main() {
    app = express()
    app.use(cors(corsOptions))
    app.use(express.json())

    connectToMongo()
    app.use(express.static(path.join(__dirname, "Views")))
    server = http.createServer(app)
    initializeControllers(app)
    seed.seedDB();
    server.listen(3001, () => {
        console.log("APP IS LISTENING ON PORT 3001!");
    });

}

function initializeControllers(app) {
    const productsController = new ProductsController(app);
    const cartController = new CartController(app);
}

function connectToMongo() {
    mongoose
        .connect("mongodb+srv://mongodb:mongodb@cluster0.od5ccku.mongodb.net/shoppingCart", {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        })
        .then(() => {
            console.log("MONGO CONNECTION TO shoppingCart !!!");
        })
        .catch((err) => {
            console.log("OH NO MONGO shoppingCart CONNECTION ERROR!!!!");
            console.log(err);
        });
}

main()